# SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>
#
# SPDX-License-Identifier: GPL-3.0-or-later

.PHONY: build
build: clean-build
	@echo '###'
	@echo 'Creating build folder...'
	@mkdir -p ./build
	@echo 'Copying extension files...'
	@cp -rv ./extension/* ./build
	@echo 'Copying licenses...'
	@cp -rv ./LICENSES ./build
	@echo 'Done.'

.PHONY: dist
dist:
	@echo '###'
	@echo 'Creating dist folder...'
	@mkdir -p ./dist
	@echo 'Creating extension bundle...'
	@cd ./build && zip -r -FS ../dist/bookmarks-start-page@romainvigier.fr.xpi *
	@echo 'Done.'

.PHONY: clean
clean: clean-build clean-dist

.PHONY: clean-build
clean-build:
	@echo '###'
	@echo 'Cleaning old build files...'
	@rm -rf ./build/*
	@echo 'Done.'

.PHONY: clean-dist
clean-dist:
	@echo '###'
	@echo 'Cleaning old dist files...'
	@rm -rf ./dist/*
	@echo 'Done.'
