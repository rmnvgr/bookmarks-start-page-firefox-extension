// SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { Bookmarks } from '../modules/bookmarks.js';
import { Icon } from '../modules/icon.js';

export class LinkList extends HTMLElement {

	constructor() {
		super();
		this._build();
		this._style();
	}

	fade() {
		this.classList.add('blurred');
	}

	unfade() {
		this.classList.remove('blurred');
	}

	async _build() {
		this.attachShadow({ mode: 'open' });

		const folderId = await self.config.getBookmarkFolderId();
		if ( !folderId ) return;

		const bookmarks = await Bookmarks.getFromFolder(folderId);
		bookmarks.forEach(async bookmark => {
			const link = document.createElement('a');
			link.classList.add('link');
			link.setAttribute('href', bookmark.url);
			this.shadowRoot.append(link);

			const icon = document.createElement('span');
			icon.classList.add('icon');
			link.append(icon);

			const title = document.createElement('span');
			title.classList.add('title');
			title.textContent = bookmark.title.length < 24 ? bookmark.title : `${bookmark.title.slice(0, 20)}…`;
			link.append(title);

			let iconUrl = new URL('images/icon.svg', window.location);
			try {
				iconUrl = await Icon.fromURL(bookmark.url);
			} catch (e) {}
			const iconImage = new Image();
			iconImage.src = iconUrl;
			iconImage.alt = '';
			if (!( await Icon.isNice(iconImage))) iconImage.classList.add('prettify');
			icon.append(iconImage);
		});
	}

	_style() {
		const style = document.createElement('style');
		style.textContent = `
:host
{
	display: grid;
	grid-template-columns: repeat(auto-fit, 7em);
	justify-content: center;
	align-content: center;
	gap: 2em 1em;
	box-sizing: border-box;
	max-width: 60vw;
	height: 100%;
	margin: 0 auto;
	filter: blur(0);
	transition: filter 0.2s;
}

:host(.blurred)
{
	filter: blur(0.2em);
}

.link
{
	display: grid;
	grid-template-rows: 5em 1em;
	grid-template-columns: 7em;
	align-items: center;
	gap: 0.5em;
	color: inherit;
	text-decoration: none;
	outline: none;
}

.title
{
	display: block;
	font-size: 0.9em;
	text-align: center;
	overflow-wrap: break-word;
}

.icon
{
	display: block;
	overflow: hidden;
	width: 5em;
	height: 5em;
	margin: 0 auto;
	border-radius: 0.2em;
	background-color: #fff;
	box-shadow: 0 0 0.5em 0.1em var(--shadow-border);
	transition: 0.1s box-shadow;
}

.link:hover .icon,
.link:focus .icon
{
	box-shadow: 0 0 1em 0.5em var(--shadow-focus);
}

.link:focus .icon
{
	outline: 2px solid var(--color);
}

.icon img
{
	box-sizing: border-box;
	display: block;
	width: 100%;
	height: 100%;
	object-fit: scale-down;
}

.icon img.prettify {
	padding: 0.4em;
}`;
		this.shadowRoot.append(style);
	}

}
