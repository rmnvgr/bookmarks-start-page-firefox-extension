// SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { Bookmarks } from '../modules/bookmarks.js';
import { FolderLink } from './folderlink.js';

export class FolderList extends HTMLElement {

	constructor(id) {
		super();

		this.folderId = id;

		this._build();
		this._style();
	}

	async _build() {
		this.attachShadow({ mode: 'open' });

		const folders = await Bookmarks.getFolders(this.folderId);
		folders.forEach(async folder => {
			const link = new FolderLink(folder);
			this.shadowRoot.append(link);
		});
	}

	_style() {
		const style = document.createElement('style');
		style.textContent = `
:host
{
	display: block;
}
		`;
		this.shadowRoot.append(style);
	}

}
