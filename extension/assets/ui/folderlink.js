// SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { Bookmarks } from '../modules/bookmarks.js';
import { FolderList } from './folderlist.js';

export class FolderLink extends HTMLElement {

	constructor(folder) {
		super();

		this.folder = folder;

		this._build();
		this._style();
	}

	async _build() {
		this.attachShadow({ mode: 'open' });

		const link = document.createElement('a');
		link.classList.add('link');
		link.textContent = `${this.folder.title} `;
		link.addEventListener('click', async () => {
			await self.config.setBookmarkFolderId(this.folder.id);
			self.ui.closePrefsPopup();
		});
		this.shadowRoot.append(link);

		const count = document.createElement('span');
		count.classList.add('count');
		count.textContent = (await Bookmarks.getFromFolder(this.folder.id)).length;
		link.append(count);

		const children = new FolderList(this.folder.id);
		children.classList.add('children');
		this.shadowRoot.append(children);
	}

	_style() {
		const style = document.createElement('style');
		style.textContent = `
:host
{
	display: block;
}

.link
{
	display: grid;
	grid-template-columns: 1fr auto;
	gap: 1em;
	padding: 0.5em 1em;
	border-bottom: 0.1em solid var(--shadow-border);
	cursor: pointer;
}

.link:hover,
.link:focus
{
	background: var(--focus-background);
}

.link:active
{
	color: var(--background);
	background: var(--color);
}

.count
{
	padding: 0.1em 0.5em;
	border-radius: 0.5em;
	background: var(--shadow-border);
	font-size: 0.8em;
}

.children
{
	margin-left: 2em;
}
		`;
		this.shadowRoot.append(style);
	}

}
