// SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { FolderList } from './folderlist.js';

export class PrefsPopup extends HTMLElement {

	constructor() {
		super();
		this._build();
		this._style();
	}

	async _build() {
		this.attachShadow({ mode: 'open' });

		const background = document.createElement('a');
		background.classList.add('background');
		background.addEventListener('click', () => {
			self.ui.closePrefsPopup();
		});
		this.shadowRoot.append(background);

		const box = document.createElement('section');
		box.classList.add('box');
		this.shadowRoot.append(box);

		const description = document.createElement('p');
		description.classList.add('description');
		description.textContent = browser.i18n.getMessage('folderSelection');
		box.append(description);

		const list = new FolderList();
		list.classList.add('list');
		box.append(list);

		const support = document.createElement('a');
		support.classList.add('support');
		support.href = 'https://liberapay.com/rmnvgr/donate/';
		support.textContent = browser.i18n.getMessage('supportDevelopment');
		box.append(support);
	}

	_style() {
		const style = document.createElement('style');
		style.textContent = `
:host
{
	display: flex;
	justify-content: center;
	align-items: center;
	box-sizing: border-box;
	position: absolute;
	top: 0;
	left: 0;
	bottom: 0;
	right: 0;
}

.background
{
	z-index: 0;
	position: absolute;
	top: 0;
	left: 0;
	bottom: 0;
	right: 0;
}

.box
{
	z-index: 100;
	padding: 2em;
	border-radius: 0.2em;
	background: var(--background);
	box-shadow: 0 0 1em 0.5em var(--shadow-border);
}

.description
{
	margin: 0 0 1em;
	font-weight: bold;
}

.support
{
	display: inline-block;
	margin: 2em 0 0;
	padding: 1em;
	border: 1px solid var(--shadow-border);
	color: var(--color);
	text-align: center;
	text-decoration: none;
}

.support::before
{
	content: '♥';
	padding-right: 0.5em;
	color: red;
}

.support:hover,
.support:focus
{
	background-color: var(--focus-background);
}
		`;
		this.shadowRoot.append(style);
	}

}
