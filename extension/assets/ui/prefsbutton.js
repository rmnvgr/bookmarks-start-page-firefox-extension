// SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

export class PrefsButton extends HTMLElement {

	constructor() {
		super();
		this._build();
		this._connect();
		this._style();
	}

	_build() {
		this.tabIndex = 0;
		this.attachShadow({ mode: 'open' });
	}

	_connect() {
		this.addEventListener('click', () => self.ui.prefsPopup ? self.ui.closePrefsPopup() : self.ui.openPrefsPopup());
	}

	_style() {
		const style = document.createElement('style');
		style.textContent = `
:host
{
	z-index: 50;
	display: block;
	position: absolute;
	bottom: 1em;
	right: 1em;
	width: 0;
	height: 2em;
	padding-left: 2em;
	overflow: hidden;
	background: var(--shadow-border);
	mask: url(./images/prefs.svg) 0 0/2em 2em;
	cursor: pointer;
}

:host(:hover),
:host(:focus)
{
	background: var(--shadow-focus);
}
		`;
		this.shadowRoot.append(style);
	}

}
