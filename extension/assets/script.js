// SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

'use strict';

import { Cache } from './modules/cache.js';
import { Config } from './modules/config.js';

import { FolderLink } from './ui/folderlink.js';
import { FolderList } from './ui/folderlist.js';
import { LinkList } from './ui/linklist.js';
import { PrefsButton } from './ui/prefsbutton.js';
import { PrefsPopup } from './ui/prefspopup.js';
import { UI } from './ui/ui.js';


async function init() {
	customElements.define('ext-folder-link', FolderLink);
	customElements.define('ext-folder-list', FolderList);
	customElements.define('ext-link-list', LinkList);
	customElements.define('ext-prefs-button', PrefsButton);
	customElements.define('ext-prefs-popup', PrefsPopup);
	customElements.define('ext-ui', UI);

	self.config = new Config();
	self.ui = new UI();
	Cache.clean();
}


init();
