// SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { Cache } from './cache.js';

export class Icon {

	/**
	 * Search a website for an icon.
	 * @param {URL} url URL of the website to search an icon for.
	 * @returns {Promise<URL>} URL of the found icon.
	 */
	static async fromURL(url) {
		try {
			return new URL(await Cache.get(url));
		} catch (e) {}
		try {
			return await getFromWebmanifest(url);
		} catch (e) {}
		try {
			return await getFromDOM(url);
		} catch (e) {}
		try {
			return await getFromFavicon(url);
		} catch (e) {}
		throw new Error(`No favicon found for ${url}.`);
	}

	/**
	 * Convert an icon to a data URL.
	 * @param {URL} iconUrl URL of the icon.
	 * @returns {Promise<URL>} The data URL.
	 */
	static toDataURL(iconUrl) {
		return new Promise((resolve, reject) => {
			const image = new Image();
			image.src = iconUrl;
			image.addEventListener('error', () => reject('Error while loading the favicon.'));
			image.addEventListener('load', () => resolve(convertImageToDataUrl(image)));
		});
	}

	/**
	 * Check if an image can be nicely displayed as an icon.
	 * @param {HTMLImageElement} image The image to check.
	 * @returns {Promise<boolean>} If the image is nice.
	 */
	static async isNice(image) {
		try {
			return !!(await Cache.get(image.src));
		} catch(e) {}
		return new Promise(resolve => {
			image.addEventListener('load', () => {
				const canvas = document.createElement('canvas');
				canvas.width = image.naturalWidth;
				canvas.height = image.naturalHeight;
				const context = canvas.getContext('2d');
				context.drawImage(image, 0, 0, canvas.width, canvas.height);
				const originAlpha = context.getImageData(0, 0, 1, 1).data[3];
				for (let x = 0; x < canvas.width; x++) {
					const topAlpha = context.getImageData(x, 0, 1, 1).data[3];
					const bottomAlpha = context.getImageData(x, canvas.height - 1, 1, 1).data[3];
					if (topAlpha !== originAlpha || bottomAlpha !== originAlpha) {
						Cache.add(image.src, '');
						resolve(false);
						return;
					}
				}
				for (let y = 0; y < canvas.height; y++) {
					const leftAlpha = context.getImageData(0, y, 1, 1).data[3];
					const rightAlpha = context.getImageData(canvas.width - 1, y, 1, 1).data[3];
					if (leftAlpha !== originAlpha || rightAlpha !== originAlpha) {
						Cache.add(image.src, '');
						resolve(false);
						return;
					}
				}
				Cache.add(image.src, 'nice');
				resolve(true);
			});
		});
	}

}

async function getFromWebmanifest(url) {
	const response = await fetch(url, {
		'referrerPolicy': 'no-referrer',
		'credentials': 'omit',
	});
	const parser = new DOMParser();
	const dom = parser.parseFromString(await response.text(), response.headers.get('Content-Type').split(';')[0]);
	const linkManifest = dom.querySelector('link[rel=manifest]');
	if ( !linkManifest ) throw new Error('No manifest found in the DOM.');
	const manifestUrl = new URL(linkManifest.getAttribute('href'), response.url);
	const manifestResponse = await fetch(manifestUrl, {
		'referrerPolicy': 'no-referrer'
	});
	const manifest = await manifestResponse.json();
	const iconUrl = new URL(
		manifest.icons
			.filter(icon => ['image/png', 'image/x-icon'].includes(icon.type))
			.filter(icon => icon.purpose !== 'monochrome')
			.filter(icon => icon.sizes.split(' ')[0].split('x')[0] === icon.sizes.split(' ')[0].split('x')[1])
			.sort((a, b) => Number(a.sizes.split(' ')[0].split('x')[0]) < Number(b.sizes.split(' ')[0].split('x')[0]))[0]
			.src,
		manifestResponse.url);
	const backgroundColor = manifest.background_color;
	const dataUrl = await Icon.toDataURL(iconUrl, backgroundColor);
	Cache.add(url, dataUrl.href);
	return dataUrl;
}

/**
 * Search the DOM of a HTML document for a favicon URL.
 * @param {URL} url URL of the website to search a favicon for.
 * @returns {Promise<URL>} URL of the found favicon.
 */
async function getFromDOM(url) {
	const response = await fetch(url, {
		'referrerPolicy': 'no-referrer',
		'credentials': 'omit',
	});
	const parser = new DOMParser();
	const dom = parser.parseFromString(await response.text(), 'text/html');
	let linkIcon;
	let linkIcons = dom.querySelectorAll('link[rel="apple-touch-icon"]');
	if ( linkIcons.length === 0 ) linkIcons = dom.querySelectorAll('link[rel="icon"]');
	if ( linkIcons.length === 0 ) linkIcons = dom.querySelectorAll('link[rel="shortcut icon"]');
	if ( linkIcons.length === 0 ) throw new Error('No icon found in the DOM.');
	if ( linkIcons.length === 1 ) {
		linkIcon = linkIcons[0];
	} else {
		try {
			linkIcon = Array.from(linkIcons)
				.filter(icon => icon.sizes[0].split('x')[0] === icon.sizes[0].split('x')[1])
				.sort((a, b) => Number(a.sizes[0].split('x')[0]) < Number(b.sizes[0].split('x')[0]))[0];
		} catch (e) {
			linkIcon = linkIcons[0];
		}
	}
	const faviconUrl = new URL(linkIcon.getAttribute('href'), response.url);
	const dataUrl = await Icon.toDataURL(faviconUrl);
	Cache.add(url, dataUrl.href);
	return dataUrl;
}

/**
 * Query the default favicon location.
 * @param {URL} url URL of the website to search a favicon for.
 * @returns {Promise<URL>} URL of the found favicon.
 */
async function getFromFavicon(url) {
	const faviconUrl = new URL('/favicon.ico', url);
	const response = await fetch(faviconUrl, {
		'referrerPolicy': 'no-referrer'
	});
	if ( !response.ok ) throw new Error('No favicon found on the server.');
	const dataUrl = await Icon.toDataURL(faviconUrl);
	Cache.add(url, dataUrl.href);
	return dataUrl;
}

/**
 * Convert an image to a data URL.
 * @param {HTMLImageElement} image The image to convert.
 * @returns {URL} The data URL.
 */
function convertImageToDataUrl(image) {
	const canvas = document.createElement('canvas');
	canvas.width = image.naturalWidth;
	canvas.height = image.naturalHeight;
	canvas.getContext('2d').drawImage(image, 0, 0, canvas.width, canvas.height);
	return new URL(canvas.toDataURL());
}
