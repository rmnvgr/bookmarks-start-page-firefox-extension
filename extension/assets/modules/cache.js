// SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

export class Cache {

	/**
	 * Retrieve data from the cache for a given key.
	 * @param {String} key The key of the cached data.
	 * @returns {Promise<String>} The retrieved data.
	 */
	static async get(key) {
		const internalKey = makeCacheKey(key);
		const cache = await browser.storage.local.get(internalKey);
		if ( !cache[internalKey] ) throw new Error('Data not found in cache.');
		if ( !isFresh(new Date(cache[internalKey].date), 30) ) throw new Error('Cache is outdated.');
		return cache[internalKey].data;
	}

	/**
	 * Cache data at a given key.
	 * @param {String} key The key of the cached data.
	 * @param {String} data The data to cache.
	 */
	static async add(key, data) {
		const internalKey = makeCacheKey(key);
		const cache = {};
		cache[internalKey] = {
			data: data.toString(),
			date: Date.now()
		};
		await browser.storage.local.set(cache);
	}

	/**
	 * Clean the cache of any outdated data.
	 */
	static async clean() {
		const cache = await browser.storage.local.get();
		Object.keys(cache).forEach(key => {
			if ( !key.match(/^cache-/) ) return;
			if ( !isFresh(new Date(cache[key].date), 30) ) {
				browser.storage.local.remove(key);
			}
		});
	}

}


/**
 * Check if a cache date is still valid.
 * @param {Date} date The date of the cache.
 * @param {Number} maxAge Maximum possible age of the cache, in days.
 * @returns {Boolean}
 */
function isFresh(date, maxAge) {
	return ((Date.now() - date) / 1000 / 3600 / 24) < maxAge;
}

/**
 * Create a cache key from a string.
 * @param {String} str The string to make into a cache key.
 * @returns {String} The created cache key.
 */
function makeCacheKey(str) {
	return `cache-${str}`;
}
