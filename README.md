<!--
SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# Bookmarks start page

Show the content of a bookmarks folder when opening a new window or a new tab.

![](res/screenshot1.png)

## Download

[Download the extension for Firefox from the Mozilla Marketplace](https://addons.mozilla.org/fr/firefox/addon/bookmarks-start-page/)

## Build the extension

```bash
make && make dist
```

This will create a `bookmarks-start-page@romainvigier.fr.xpi` file in the `dist` folder.

## Support

To support the development of this extension, you can [donate via Liberapay](https://liberapay.com/rmnvgr/donate/).
